import { createSlice, configureStore } from '@reduxjs/toolkit';

const initialCounterState = {
  counter: 0,
  showCounter: true,
  years: [],
  weeks: [],
  diseases: [],
  selectedYear: '2013',
  selectedWeek: '',

  searchDiseases: [],
  filteredDiseases: '',
  filterDiseasesOptions: [],
};

const counterSlice = createSlice({
  name: 'counter',
  initialState: initialCounterState,
  reducers: {
    setYears(state, action) {
      state.years = action.payload;
    },
    setWeeks(state, action) {
      state.weeks = action.payload;
    },
    setDiseases(state, action) {
      state.diseases = action.payload;
    },
    searchDiseases(state, action) {
      state.searchDiseases = action.payload;
    },
    filteredDiseases(state, action) {
      state.filteredDiseases = action.payload;
    },

    selectedYear(state, action) {
      state.selectedYear = action.payload;
    },
    selectedWeek(state, action) {
      state.selectedWeek = action.payload;
    },
    filterDiseasesOptions(state, action) {
      state.filterDiseasesOptions = action.payload;
    },
  },
});

const store = configureStore({
  reducer: { counter: counterSlice.reducer },
});

export const filterActions = counterSlice.actions;
export default store;
