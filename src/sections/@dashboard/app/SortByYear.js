import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
// material
import { Menu, Button, MenuItem, Typography } from '@mui/material';
// component
import Iconify from '../../../components/Iconify';
import { filterActions } from '../../../store/index';
// ----------------------------------------------------------------------

export default function SortYear() {
  const dispatch = useDispatch();
  const years = useSelector((state) => state.counter.years);
  const selectedYear = useSelector((state) => state.counter.selectedYear);

  // Function to collect data

  useEffect(() => {
    const fetchData = async () => {
      await fetch(`${process.env.REACT_APP_URL}/api/v1/datastore/years`)
        .then((response) => response.json())
        .then((data) => dispatch(filterActions.setYears(data.data)));
    };
    // call the function
    fetchData().catch(console.error);
  }, [dispatch]);

  const [open, setOpen] = useState(null);

  const handleOpen = (event) => {
    setOpen(event.currentTarget);
  };

  const handleClose = async (e, option) => {
    if (option !== 'backdropClick') {
      dispatch(filterActions.selectedYear(option));
      dispatch(filterActions.selectedWeek(''));
    }
    setOpen(null);
  };

  return (
    <>
      <Button
        color="inherit"
        disableRipple
        onClick={handleOpen}
        id="test-year"
        endIcon={<Iconify icon={open ? 'eva:chevron-up-fill' : 'eva:chevron-down-fill'} />}
      >
        By Year: &nbsp;
        <Typography component="span" variant="subtitle2" sx={{ color: 'text.secondary' }}>
          {selectedYear}
        </Typography>
      </Button>
      <Menu
        keepMounted
        anchorEl={open}
        open={Boolean(open)}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        {years.map((option) => (
          <MenuItem
            key={option.value}
            selected={option.value === 'newest'}
            onClick={(e) => handleClose(e, option.value)}
            sx={{ typography: 'body2' }}
          >
            {option.label}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
}
