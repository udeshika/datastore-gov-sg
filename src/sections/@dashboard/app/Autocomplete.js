import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import { filterActions } from '../../../store/index';

export default function ComboBox() {
  const dispatch = useDispatch();
  const filterDiseasesOptions = useSelector((state) => state.counter.filterDiseasesOptions);
  // Function to collect data

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(),
    };
    const fetchData = async () => {
      await fetch(`${process.env.REACT_APP_URL}/api/v1/datastore/diseases`, requestOptions)
        .then((response) => response.json())
        .then((data) => dispatch(filterActions.filterDiseasesOptions(data.data)));
    };
    // call the function
    fetchData().catch(console.error);
  }, [dispatch]);
  // filter close event
  const handleClose = async (event, option) => {
    dispatch(filterActions.filteredDiseases(option));
  };
  return (
    <Autocomplete
      disablePortal
      id="combo-box-demo"
      options={filterDiseasesOptions}
      onInputChange={handleClose}
      sx={{ width: 300 }}
      renderInput={(params) => <TextField {...params} label="Diseases" />}
    />
  );
}
