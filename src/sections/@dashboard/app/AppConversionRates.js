import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import merge from 'lodash/merge';
import ReactApexChart from 'react-apexcharts';
// @mui
import { Box } from '@mui/material';
// utils
import { fNumber } from '../../../utils/formatNumber';
// components
import { BaseOptionChart } from '../../../components/chart';
import { filterActions } from '../../../store/index';
// ----------------------------------------------------------------------

export default function AppConversionRates() {
  const dispatch = useDispatch();
  const chartData = useSelector((state) => state.counter.diseases);
  // Function to collect data
  const selectedWeek = useSelector((state) => state.counter.selectedWeek);
  const selectedYear = useSelector((state) => state.counter.selectedYear);
  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ year: selectedYear, week: selectedWeek }),
    };
    fetch(`${process.env.REACT_APP_URL}/api/v1/datastore/search`, requestOptions)
      .then((response) => response.json())
      .then((data) => dispatch(filterActions.setDiseases(data.data)));
  }, [dispatch, selectedYear, selectedWeek]);

  console.log(chartData);
  const chartLabels = chartData.map((i) => i.label);

  const chartSeries = chartData.map((i) => i.value);

  const chartOptions = merge(BaseOptionChart(), {
    tooltip: {
      marker: { show: false },
      y: {
        formatter: (seriesName) => fNumber(seriesName),
        title: {
          formatter: () => '',
        },
      },
    },
    plotOptions: {
      bar: { horizontal: true, barHeight: '28%', borderRadius: 2 },
    },
    xaxis: {
      categories: chartLabels,
    },
  });

  return (
    <Box sx={{ mx: 3 }} dir="ltr">
      <ReactApexChart type="bar" series={[{ data: chartSeries }]} options={chartOptions} height={1000} />
    </Box>
  );
}
