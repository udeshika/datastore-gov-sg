import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
// material
import { Menu, Button, MenuItem, Typography } from '@mui/material';
// component
import Iconify from '../../../components/Iconify';
import { filterActions } from '../../../store/index';
// ----------------------------------------------------------------------

export default function SortWeek() {
  const dispatch = useDispatch();
  const weeks = useSelector((state) => state.counter.weeks);
  const selectedWeek = useSelector((state) => state.counter.selectedWeek);
  const selectedYear = useSelector((state) => state.counter.selectedYear);
  // Function to collect data

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ year: selectedYear }),
    };
    const fetchData = async () => {
      await fetch(`${process.env.REACT_APP_URL}/api/v1/datastore/weeks`, requestOptions)
        .then((response) => response.json())
        .then((data) => dispatch(filterActions.setWeeks(data.data)));
    };
    // call the function
    fetchData().catch(console.error);
  }, [dispatch, selectedYear]);

  const [open, setOpen] = useState(null);

  const handleOpen = (event) => {
    setOpen(event.currentTarget);
  };

  const handleClose = async (e, option) => {
    if (option !== 'backdropClick') {
      dispatch(filterActions.selectedWeek(option));
    }
    setOpen(null);
  };

  return (
    <>
      <Button
        color="inherit"
        disableRipple
        onClick={handleOpen}
        endIcon={<Iconify icon={open ? 'eva:chevron-up-fill' : 'eva:chevron-down-fill'} />}
      >
        By Weeks:&nbsp;
        <Typography component="span" variant="subtitle2" sx={{ color: 'text.secondary' }}>
          {selectedWeek}
        </Typography>
      </Button>
      <Menu
        keepMounted
        anchorEl={open}
        open={Boolean(open)}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        {weeks.map((option) => (
          <MenuItem
            key={option.value}
            selected={option.value === ''}
            onClick={(e) => handleClose(e, option.value)}
            sx={{ typography: 'body2' }}
          >
            {option.label}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
}
