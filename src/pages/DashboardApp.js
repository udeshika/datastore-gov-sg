// @mui
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { Grid, Container } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';

// sections
import { AppConversionRates, SortByWeek, SortByYear, Autocomplete } from '../sections/@dashboard/app';
// components
import Page from '../components/Page';

import { filterActions } from '../store/index';

// ----------------------------------------------------------------------

export default function DashboardApp() {
  const dispatch = useDispatch();
  const filteredDiseases = useSelector((state) => state.counter.filteredDiseases);
  const searchDiseases = useSelector((state) => state.counter.searchDiseases);

  // Function to collect data

  useEffect(() => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ diseases: filteredDiseases }),
    };

    const fetchData = async () => {
      await fetch(`${process.env.REACT_APP_URL}/api/v1/datastore/weekly_data`, requestOptions)
        .then((response) => response.json())
        .then((data) => dispatch(filterActions.searchDiseases(data.data)));
    };
    // call the fetchData function
    fetchData().catch(console.error);
  }, [dispatch, filteredDiseases]);

  return (
    <Page
      title="Tech Assignment
Building A Infectious Disease Bulletin"
    >
      <Container maxWidth="xl">
        <Grid container spacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <Card>
              <CardContent>
                <SortByYear />
                <SortByWeek />
                <AppConversionRates title="Tech Assignment" subheader="" />
              </CardContent>
            </Card>
          </Grid>

          <Grid item xs={12} md={6} lg={6}>
            <Card>
              <CardContent>
                <Autocomplete />
                <TableContainer component={Paper}>
                  <Table aria-label="simple table" height={500}>
                    <TableHead>
                      <TableRow>
                        <TableCell>Diseases</TableCell>
                        <TableCell align="right">No of cases</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {searchDiseases.map((diseases) => (
                        <TableRow key={diseases.label} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                          <TableCell component="th" scope="row">
                            {diseases.label}
                          </TableCell>
                          <TableCell align="right">{diseases.value}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </TableContainer>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
}
